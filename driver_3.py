# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 10:54:04 2017

@author: evimar
"""
import os
import csv
import pandas as pd
import re
#from functools import reduce
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
import numpy as np

train_path = "./aclImdb/train/"
test_path = "imdb_te.csv" # test data for grade evaluation in my PC

train_path = "../resource/asnlib/public/aclImdb/train/" # use terminal to ls files under this directory
test_path = "../resource/asnlib/public/imdb_te.csv" # test data for grade evaluation


datafile = "imdb_tr.csv"

split_regex = r'\W+'
stopfile = 'stopword2.txt'

#Keep join the words with apostrohpe
split_regex = r"(?:(?:[^a-zA-Z]+')|(?:'[^a-zA-Z]+))|(?:[^a-zA-Z']+)"
split_regex = r"(?:[^a-zA-Z']+)"

stopfile = 'stopword3.txt' #has words without apostrophe 


def simpleTokenize(string):
    """ A simple implementation of input string tokenization
    Args:
        string (str): input string
    Returns:
        list: a list of tokens
    """
    l =  re.split(split_regex,string.lower().replace("'",""))
    
    s = filter(lambda x: x!='', l)
    return s


def tokenize(string):
    """ An implementation of input string tokenization that excludes stopwords
    Args:
        string (str): input string
    Returns:
        list: a list of tokens without stopwords
    """
    lista = simpleTokenize(string)
    return list(filter(lambda x: x not in stopwords, lista ))
    
def imdb_data_preprocess(inpath, outpath="./", name="imdb_tr.csv", mix=False):
    '''Implement this module to extract
    and combine text files under train_path directory into 
    imdb_tr.csv. Each text file in train_path should be stored 
    as a row in imdb_tr.csv. And imdb_tr.csv should have three 
    columns, "row_number", "text" and label'''
    row = 0
    fn = ['row_number','text', 'label']
    with open(os.path.join(outpath, name), 'w') as output_file:
        a = csv.writer(output_file, delimiter=',')
        label = '1'
        a.writerow(fn)
        
        for file in os.listdir(inpath+'pos/'):
            if file.endswith(".txt"):
               with open(os.path.join(inpath+'pos/', file), 'r') as f:
                   review = f.readlines()
                   
               a.writerow([str(row), review[0], label])
               row += 1

        label = '0'
        for file in os.listdir(inpath+'neg/'):
            if file.endswith(".txt"):
               with open(os.path.join(inpath+'neg/', file), 'r') as f:
                   review = f.readlines()
                   
               a.writerow([str(row), review[0], label])
               row += 1

def NLPClassifier(pipeline, Xtrain, label, Xtest, outputfile):
    """
    """   
    _ = pipeline.fit(Xtrain, label)
    predicted = pipeline.predict(Xtest)
    print(outputfile)
    print(np.mean(pipeline.predict(Xtrain)==label))
    np.savetxt(outputfile, predicted, fmt='%d')

  
if __name__ == "__main__":
#Union of all txt in one csv file    
    imdb_data_preprocess(train_path)

#Read the cvs 
    raw_data = pd.read_csv(datafile, delimiter=',', index_col=0)
    text_noapos = raw_data.apply(lambda x: x.text.replace("'",""), axis=1)
#Read test file
    raw_test = pd.read_csv(test_path, index_col=0, header=0, encoding="ISO-8859-1")   
    raw_test_noapos = raw_test.apply(lambda x: x.text.replace("'",""), axis=1)
    
#Read the stopwords file
    with open(stopfile, 'r') as sf:
        stopwords = {l.replace("\n","") for l in sf.readlines()}

##Tokenize each row only relevant words       
#    data_token =   raw_data.apply(lambda x: tokenize(x.text), axis=1)  
#    
##Create a vocabulary set with only relevant words    
#    vocabulary = reduce(lambda x,y: set.union(x,y), data_token.apply(lambda x:    set(x))  )
#    
#    N = len(vocabulary)
#    #Clase0 and Clase1 are boolean cols that maps its classes
#    Clase0, Clase1 = raw_data.label==0, raw_data.label==1
#    Ndocs_0 = (Clase0).sum() #Nros of docs in the class
#    Ndocs_1 = (Clase1).sum()
#    Ndocs = raw_data.label.count() #Total docs in Dataset  
#    
#    #Probability of the classes
#    P_0 = Ndocs_0 / Ndocs
#    P_1 = Ndocs_1 / Ndocs
#    
#    #Number of words in every class
#    Nwords_0 = reduce(lambda x,y: x+y, data_token[Clase0].apply(lambda x: len(x)))
#    Nwords_1 = reduce(lambda x,y: x+y, data_token[Clase1].apply(lambda x: len(x)))
#    
    
    '''train a SGD classifier using unigram representation,
    predict sentiments on imdb_te.csv, and write output to
    unigram.output.txt'''
    UnigramPipeline = Pipeline([('vect', CountVectorizer
                        (analyzer='word', ngram_range=(1,1), stop_words=stopwords, 
                         strip_accents='ascii', min_df=30, max_df=0.9, 
                         token_pattern=r"(?:[a-zA-Z']+)")),
                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                            alpha=1e-3, n_iter=5, n_jobs=-1)) ])

    
    NLPClassifier(UnigramPipeline, text_noapos, raw_data.label, raw_test_noapos, 
                  'unigram.output.txt')

    '''train a SGD classifier using bigram representation,
    predict sentiments on imdb_te.csv, and write output to
    bigram.output.txt '''
    BigramPipeline = Pipeline([('vect', CountVectorizer
                        (analyzer='word', ngram_range=(1,2), stop_words=stopwords, 
                         strip_accents='ascii', min_df=30, max_df=0.9, 
                         token_pattern=r"(?:[a-zA-Z']+)")),
                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                            alpha=1e-3, n_iter=5, n_jobs=-1)) ])

    NLPClassifier(BigramPipeline, text_noapos, raw_data.label, raw_test_noapos, 
                  'bigram.output.txt')
     
    '''train a SGD classifier using unigram representation
     with tf-idf, predict sentiments on imdb_te.csv, and write 
     output to unigramtfidf.output.txt'''
#    TfIdfUnigramPipeline = Pipeline([('vect', TfidfVectorizer
#                        (analyzer='word', ngram_range=(1,1), stop_words=stopwords, 
#                         strip_accents='ascii', min_df=30, max_df=0.9, 
#                         token_pattern=r"(?:[a-zA-Z']+)", sublinear_tf=True)),
#                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
#                                            alpha=1e-3, n_iter=5, n_jobs=-1)) ])

    TfIdfUnigramPipeline = Pipeline([('vect', TfidfVectorizer
                        (ngram_range=(1,1), sublinear_tf=True)),
                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                            n_iter=5, n_jobs=-1)) ])
                                            
    NLPClassifier(TfIdfUnigramPipeline, raw_data.text, raw_data.label, raw_test, 
                  'unigramtfidf.output.txt')

     	
    '''train a SGD classifier using bigram representation
     with tf-idf, predict sentiments on imdb_te.csv, and write 
     output to bigramtfidf.output.txt'''

    TfIdfBigramPipeline = Pipeline([('vect', TfidfVectorizer
                        (ngram_range=(1,2), sublinear_tf=True)),
                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                            n_iter=5, n_jobs=-1)) ])

    
    NLPClassifier(TfIdfBigramPipeline, raw_data.text, raw_data.label, raw_test, 
                  'bigramtfidf.output.txt')
